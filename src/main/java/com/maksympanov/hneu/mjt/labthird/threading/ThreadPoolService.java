package com.maksympanov.hneu.mjt.labthird.threading;

import lombok.SneakyThrows;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.lang.Math.abs;
import static java.lang.System.out;

public class ThreadPoolService {

    private static final int JOBS_COUNT = 20;

    private static final long LEFT_TIME_BORDER = 5000;

    private static final long RIGHT_TIME_BORDER = 10000;

    private static final long EXECUTION_TIMEOUT = 20000;

    private final SecureRandom RND = new SecureRandom();

    @SneakyThrows
    public void executeThreadPoolExample() {

        var executor = Executors.newFixedThreadPool(5);

        var jobs = generateJobs();

        out.println("Starting execution of thread pool demo");

        executor.invokeAll(jobs);
        executor.shutdown();

        var timeoutNotExpired = executor.awaitTermination(EXECUTION_TIMEOUT, TimeUnit.MILLISECONDS);
        if (timeoutNotExpired) {
            out.println("Execution of thread pool demo completed");
        } else {
            out.printf("Timeout %dms expired!%n", EXECUTION_TIMEOUT);
        }
    }

    private ArrayList<Callable<Void>> generateJobs() {
        var jobs = new ArrayList<Callable<Void>>();
        for (int i = 1; i <= JOBS_COUNT; ++i) {
            final var index = i;
            jobs.add(() -> {
                var threadName = Thread.currentThread().getName();
                var idleTime = LEFT_TIME_BORDER + abs(RND.nextInt()) % (RIGHT_TIME_BORDER - LEFT_TIME_BORDER);
                out.printf("Thread '%s' will be executing job #%d for %dms%n", threadName, index, idleTime);
                Thread.sleep(idleTime);
                out.printf("Thread '%s' has finished executing job #%d%n", threadName, index);
                return null;
            });
        }
        return jobs;
    }

}
