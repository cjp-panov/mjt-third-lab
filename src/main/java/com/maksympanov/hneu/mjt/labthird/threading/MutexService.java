package com.maksympanov.hneu.mjt.labthird.threading;

import lombok.SneakyThrows;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Math.abs;
import static java.lang.System.*;

public class MutexService {

    private final Scanner IN = new Scanner(in);

    private final SecureRandom RND = new SecureRandom();

    private final Lock mutex = new ReentrantLock();

    @SneakyThrows
    public void executeMutexExample() {
        out.print("Enter number of threads: ");
        var threadCount = IN.nextInt();
        out.print("Enter left time border (ms): ");
        var t1 = IN.nextLong();
        out.print("Enter right time border (ms): ");
        var t2 = IN.nextLong();

        if (IOUtils.isTimeInputInvalid(t1, t2)) {
            return;
        }

        var threads = new ArrayList<Thread>();
        IN.nextLine();
        for (int i = 1; i <= threadCount; ++i) {
            out.printf("Enter name of thread #%s: ", i);
            var threadName = IN.nextLine();
            var lockTime = t1 + abs(RND.nextLong()) % (t2 - t1);
            threads.add(new Thread(() -> threadMutexLogic(lockTime), threadName));
        }

        out.println("\nMutex concurrency simulation started");

        threads.forEach(Thread::start);

        for (var thread : threads) {
            thread.join();
        }

        out.println("Mutex concurrency simulation completed\n");

    }

    private void threadMutexLogic(long lockTime) {
        var threadName = Thread.currentThread().getName();
        mutex.lock();
        out.printf("Thread '%s' acquiring the mutex for %dms%n", threadName, lockTime);

        try {
            Thread.sleep(lockTime);
        } catch (InterruptedException ignored) {}

        out.printf("Thread '%s' is now releasing the mutex%n", threadName);
        mutex.unlock();
    }

}
