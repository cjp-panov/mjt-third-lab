package com.maksympanov.hneu.mjt.labthird.threading;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

import static java.lang.Math.abs;
import static java.lang.System.*;
import static java.lang.System.out;

public class SemaphoreService {

    private final Scanner IN = new Scanner(in);

    private final SecureRandom RND = new SecureRandom();

    private final Semaphore semaphore = new Semaphore(4, true);

    @SneakyThrows
    public void executeSemaphoreExample() {
        out.print("Enter number of threads: ");
        var threadCount = IN.nextInt();
        out.print("Enter left time border (ms): ");
        var t1 = IN.nextLong();
        out.print("Enter right time border (ms): ");
        var t2 = IN.nextLong();

        if (IOUtils.isTimeInputInvalid(t1, t2)) {
            return;
        }

        var threads = new ArrayList<Thread>();
        IN.nextLine();
        for (int i = 1; i <= threadCount; ++i) {
            out.printf("Enter name of thread #%s: ", i);
            var threadName = IN.nextLine();
            var lockTime = t1 + abs(RND.nextLong()) % (t2 - t1);
            threads.add(new Thread(new ThreadJob(lockTime), threadName));
        }

        out.println("\nSemaphore concurrency simulation started");

        threads.forEach(Thread::start);

        for (var thread : threads) {
            thread.join();
        }

        out.println("Semaphore concurrency simulation completed\n");

    }

    @AllArgsConstructor
    private class ThreadJob implements Runnable {

        private long lockTime;

        @Override
        public void run() {
            var threadName = Thread.currentThread().getName();

            if (!semaphore.tryAcquire()) {
                out.printf("Thread '%s' could not acquire semaphore since there are no free seats%n", threadName);
                try {
                    semaphore.acquire();
                } catch (InterruptedException ignored) {}
            }

            out.printf("Thread '%s' acquired semaphore seat for %dms%n", threadName, lockTime);

            try {
                Thread.sleep(lockTime);
            } catch (InterruptedException ignored) {}

            out.printf("Thread '%s' is now releasing semaphore seat%n", threadName);
            semaphore.release();
        }

    }

}
