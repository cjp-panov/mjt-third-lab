package com.maksympanov.hneu.mjt.labthird.threading;

import java.util.Scanner;

import static java.lang.System.*;

public class Demo {

    private static final Scanner IN = new Scanner(in);

    private static final MutexService mutexService = new MutexService();

    private static final SemaphoreService semaphoreService = new SemaphoreService();

    private static final AtomicVariablesService atomicVariablesService = new AtomicVariablesService();

    private static final ThreadPoolService threadPoolService = new ThreadPoolService();

    private static final MatrixService matrixService = new MatrixService();

    public static void main(String[] args) {
        menu();
    }

    private static void menu() {
        out.println("------------- MENU -------------");
        out.println("Choose an option: ");
        out.println("1. Mutex demo");
        out.println("2. Semaphore demo");
        out.println("3. Atomic variables demo");
        out.println("4. Thread pool demo");
        out.println("5. Matrices product");
        out.println("6. Exit");
        out.println("--------------------------------");
        out.print("Your choice: ");

        var choice = IN.nextInt();

        if (choice == 6) {
            return;
        }

        switch (choice) {
            case 1 -> mutexService.executeMutexExample();
            case 2 -> semaphoreService.executeSemaphoreExample();
            case 3 -> atomicVariablesService.executeAtomicVariablesExample();
            case 4 -> threadPoolService.executeThreadPoolExample();
            case 5 -> matrixService.executeDemoMatrixProduct();
            default -> err.println("Invalid value. Please try again");
        }

        menu();
    }
}
