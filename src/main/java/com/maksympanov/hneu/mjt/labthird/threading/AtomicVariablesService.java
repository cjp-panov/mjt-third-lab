package com.maksympanov.hneu.mjt.labthird.threading;

import lombok.SneakyThrows;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Math.abs;
import static java.lang.System.out;

public class AtomicVariablesService {

    private final SecureRandom RND = new SecureRandom();

    private final AtomicInteger VALUE = new AtomicInteger(0);

    private static final int INCR_THREAD_COUNT = 7;

    private static final int DECR_THREAD_COUNT = 5;

    private static final int OPERATIONS_PER_INCR_THREAD = 4;

    private static final int OPERATIONS_PER_DECR_THREAD = 3;

    @SneakyThrows
    public void executeAtomicVariablesExample() {
        var threads = new ArrayList<Thread>();
        for (int i = 1; i <= INCR_THREAD_COUNT; ++i) {
            threads.add(
                    new Thread(
                            new IncrementAtomicThreadJob(),
                            String.valueOf(i)
                    )
            );
        }
        for (int i = 1; i <= DECR_THREAD_COUNT; ++i) {
            threads.add(
                    new Thread(
                            new DecrementAtomicThreadJob(),
                            String.valueOf(INCR_THREAD_COUNT + i)
                    )
            );
        }

        var expectedResult = INCR_THREAD_COUNT * OPERATIONS_PER_INCR_THREAD - DECR_THREAD_COUNT * OPERATIONS_PER_DECR_THREAD;
        out.printf("Expected result: %d", expectedResult);
        out.println("\nBlocking atomic concurrency simulation started");

        threads.forEach(Thread::start);

        for (var thread : threads) {
            thread.join();
        }

        out.println("Blocking atomic concurrency simulation completed\n");
        out.printf("Received result: %d", VALUE.get());

    }

    private class IncrementAtomicThreadJob implements Runnable {

        @SneakyThrows
        @Override
        public void run() {
            for (int i = 0; i < OPERATIONS_PER_INCR_THREAD; ++i) {
                Thread.sleep(abs(RND.nextInt()) % 2000);
                var threadName = Thread.currentThread().getName();
                var current = VALUE.incrementAndGet();
                out.printf("Thread '%s' incremented the value. Current value - %d%n", threadName, current);
            }
        }

    }

    private class DecrementAtomicThreadJob implements Runnable {

        @SneakyThrows
        @Override
        public void run() {
            for (int i = 0; i < OPERATIONS_PER_DECR_THREAD; ++i) {
                Thread.sleep(abs(RND.nextInt()) % 2000);
                var threadName = Thread.currentThread().getName();
                var current = VALUE.decrementAndGet();
                out.printf("Thread '%s' decremented the value. Current value - %d%n", threadName, current);
            }
        }

    }
}
