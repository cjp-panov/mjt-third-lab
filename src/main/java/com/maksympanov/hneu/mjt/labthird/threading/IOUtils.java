package com.maksympanov.hneu.mjt.labthird.threading;

import static java.lang.System.err;

public class IOUtils {

    public static boolean isTimeInputInvalid(long t1, long t2) {
        if (t1 >= t2) {
            err.printf("Left time border (%dms) cannot be greater than right border (%dms)%n", t1, t2);
            return true;
        }

        if (t1 <= 0) {
            err.println("Time borders should be positive");
            return true;
        }

        return false;
    }

    public static boolean matrixProductArgumentsAreValid(int[][] mat1, int[][] mat2, int[][] rv) {
        var n1 = mat1.length;
        if (n1 == 0) {
            err.println("Matrix dimensions should be positive numbers");
            return false;
        }
        var m1 = mat1[0].length;
        if (m1 == 0) {
            err.println("Matrix dimensions should be positive numbers");
            return false;
        }

        var n2 = mat2.length;
        if (n2 == 0) {
            err.println("Matrix dimensions should be positive numbers");
            return false;
        }
        var m2 = mat2[0].length;
        if (m2 == 0) {
            err.println("Matrix dimensions should be positive numbers");
            return false;
        }

        var nr = rv.length;
        if (nr == 0) {
            err.println("Result matrix dimensions should be positive numbers");
            return false;
        }
        var mr = rv[0].length;
        if (mr == 0) {
            err.println("Result matrix dimensions should be positive numbers");
            return false;
        }

        if (n1 != m2) {
            err.println("Number of rows in first matrix should equal to number of columns of second matrix");
            return false;
        }

        return true;
    }

    private IOUtils() {}

}
