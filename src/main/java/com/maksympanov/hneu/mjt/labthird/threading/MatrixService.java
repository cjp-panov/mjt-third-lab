package com.maksympanov.hneu.mjt.labthird.threading;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.lang.Math.abs;
import static java.lang.Math.min;
import static java.lang.System.*;
import static java.util.Arrays.stream;

public class MatrixService {

    private final Scanner IN = new Scanner(in);

    private final SecureRandom RND = new SecureRandom();

    @SneakyThrows
    public void executeDemoMatrixProduct() {

        // TODO: get from stdin
        int[][] mat1 = {
                {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
                {16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30},
                {31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45},
                {46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60},
                {61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75},
                {76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90},
                {91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105},
                {106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120},
                {121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135},
                {136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150},
                {151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165},
                {166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180},
                {181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195},
                {196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210},
                {211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225}
        };

        // TODO: get from stdin
        int[][] mat2 = {
                {10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 10, 9, 8, 7},
                {11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 11, 12, 13, 14},
                {22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 22, 23, 24, 25},
                {33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 33, 34, 35, 36},
                {44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 44, 45, 46, 47},
                {55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 55, 56, 57, 58},
                {66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 66, 67, 68, 69},
                {77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 77, 78, 79, 80},
                {88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 88, 89, 90, 91},
                {99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 99, 100, 101, 102},
                {110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 110, 111, 112, 113},
                {121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 121, 122, 123, 124},
                {132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 132, 133, 134, 135},
                {143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 143, 144, 145, 146},
                {154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 154, 155, 156, 157}
        };

        var rv = new int[15][15];
        for (int i = 0; i < 15; ++i) {
            rv[i] = new int[15];
            for (int j = 0; j < 15; ++j) {
                rv[i][j] = -1;
            }
        }

        out.print("Enter maximal number of rows that will be calculated by one working thread: ");
        var rowsPerThread = IN.nextInt();
        if (rowsPerThread <= 0) {
            err.println("Number of rows per thread should be positive number");
            return;
        }

        out.println("Result matrix before algorithm execution: ");
        stream(rv).forEach(row -> {
            Arrays.stream(row).forEach(el -> out.printf("%d ", el) );
            out.println();
        });
        matrixProduct(mat1, mat2, rv, rowsPerThread);
        out.println("\n\nResult matrix after algorithm execution: ");
        stream(rv).forEach(row -> {
            stream(row).forEach( el -> out.printf("%d ", el) );
            out.println();
        });
    }

    @SneakyThrows
    private void matrixProduct(int[][] mat1, int[][] mat2, int[][] rv, int rowsPerThread) {
        if (!IOUtils.matrixProductArgumentsAreValid(mat1, mat2, rv)) {
            return;
        }

        var mainThread = new Thread(new MatrixProductJob(mat1, mat2, rv, rowsPerThread));
        out.println("Starting matrix product operation");
        mainThread.start();
        mainThread.join();
    }

    @AllArgsConstructor
    private class MatrixProductJob implements Runnable {

        private int[][] mat1;

        private int[][] mat2;

        private int[][] rv;

        private int rowsPerThread;

        @SneakyThrows
        @Override
        public void run() {
            var rowsCount = mat1.length;
            var executor = Executors.newFixedThreadPool(5);

            for (int i = 0; i < rowsCount; i = min(i + rowsPerThread, rowsCount)) {
                var toRow = min(i + rowsPerThread, rowsCount);
                executor.submit(new MatrixProductJobWorker(mat1, mat2, rv, i, toRow));
            }

            executor.shutdown();

            if (!executor.awaitTermination(10, TimeUnit.SECONDS)) {
                executor.shutdownNow();
                err.println("Thread pool execution timeout expired!");
                return;
            }
            out.println("Successful thread pool execution");
        }

    }

    @AllArgsConstructor
    private class MatrixProductJobWorker implements Callable<Void> {

        private int[][] mat1;

        private int[][] mat2;

        private int[][] rv;

        private int fromRow;

        private int toRow;

        @Override
        public Void call() {
            var threadName = Thread.currentThread().getName();
            out.printf("Thread '%s' from the pool started evaluation of result rows %d-%d%n", threadName, fromRow, toRow);

            for (int i = fromRow; i < toRow; ++i) {
                for (int j = 0; j < rv[0].length; ++j) {
                    rv[i][j] = dotRowAndColumn(mat1, mat2, i, j);
                }
            }

            out.printf("Thread '%s' has evaluated result rows %d-%d%n", threadName, fromRow, toRow);

            return null;
        }

        @SneakyThrows
        private int dotRowAndColumn(int[][] mat1, int[][] mat2, int mat1row, int mat2col) {
            var sum = 0;
            var threadName = Thread.currentThread().getName();
            for (int i = 0; i < mat1[0].length; ++i) {
                out.printf("\t\tThread '%s' multiplying row %d of first matrix and column %d of second matrix%n", threadName, mat1row, mat2col);
                sum += mat1[mat1row][i] * mat2[i][mat2col];
                var idle = abs(RND.nextLong()) % 20;
                Thread.sleep(idle);
            }
            return sum;
        }

    }

}
